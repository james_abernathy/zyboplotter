/** @file
@brief Entry point of Plotter command interpreter.
@copyright © 2018 James Abernathy
@author James Abernathy
*/




#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "xil_printf.h"
#include "xparameters.h"
#include "xuartps.h"

// Generated driver functions for IP cores
#include "StepDriver.h"




/** The base AXI memory address of the StepDriver IP core. */
#define STEPDRIVER_AXI_BASE XPAR_STEPDRIVER0_S_AXI_BASEADDR

/** AXI bus address of X, Y, & Z go flags. relative to `STEPDRIVER_AXI_BASE`.
Write an axis' flag bit to start its movement.
*/
#define REG_AXES_GO_OFFSET (0x00)
#define REG_AXES_BUSY_OFFSET (0x04)

#define REG_FLAG_X ((uint32_t)(1 << 0))
#define REG_FLAG_Y ((uint32_t)(1 << 1))
#define REG_FLAG_Z ((uint32_t)(1 << 2))




#define UART_DEVICE_ID XPAR_XUARTPS_0_DEVICE_ID

#define UART_BAUD_RATE ((uint32_t)115200)

#define UART_RECV_BUF_LEN ((size_t)32)

/* Instance of the UART Device */
XUartPs uartPS;




/** Initializes an #XUartPs device instance.
@param[out] uart  UART device data to initialize.
@param[in] deviceId  UART device ID to configure @p uart for.
@return #XST_SUCCESS if @p uart was initialized successfully.
*/
static int UartPs_CfgInitialize_(
	XUartPs * const uart,
	const uint16_t deviceId,
	const uint32_t baudRate)
{
	// Initialize the UART driver so that it's ready to use.
	// Look up the configuration in the config table, then initialize it.
	{
		XUartPs_Config *config = XUartPs_LookupConfig(deviceId);
		if (config == NULL) {
			return XST_FAILURE;
		}

		if (XUartPs_CfgInitialize(uart, config, config->BaseAddress) != XST_SUCCESS) {
			return XST_FAILURE;
		}
	}

	if (XUartPs_SetBaudRate(uart, baudRate) != XST_SUCCESS) {
		return XST_FAILURE;
	}

	// Check hardware build
	if (XUartPs_SelfTest(uart) != XST_SUCCESS) {
		return XST_FAILURE;
	}

	return XST_SUCCESS;
}




/** Prints XYZ axis labels present within @p axisFlags.
@param[in] axisFlags  Bitfield containing X, Y, and Z flags in its least-significant bits.
*/
void printRegisterAxes(
	const uint32_t axisFlags)
{
	if (axisFlags & REG_FLAG_X) {
		print("X");
	}
	if (axisFlags & REG_FLAG_Y) {
		print("Y");
	}
	if (axisFlags & REG_FLAG_Z) {
		print("Z");
	}
}




/** Loops indefinitely, parsing serial commands.
@return	#XST_FAILURE if an error occurred.
*/
static int CommandMenuLoop_(void)
{
	uint32_t busyFlags = 0;
	uint8_t receivedBuffer[UART_RECV_BUF_LEN];

	print("Command loop: Send X, Y, or Z to trigger rotations in those axes.\n");

	while (true) {
		uint32_t receivedCount = XUartPs_Recv(
			&uartPS, receivedBuffer,
			(sizeof receivedBuffer) / (sizeof receivedBuffer[0]) - 1); // Leave room for terminator

		if (receivedCount) {
			// Parse received bytes for valid axes
			uint32_t goValue = 0;
			receivedBuffer[receivedCount] = '\0';  // Add terminator
			xil_printf("\nReceived \"%s\".\n", receivedBuffer);
			for (uint32_t byteIndex = 0; byteIndex < receivedCount; ++byteIndex) {
				const uint8_t byte = receivedBuffer[byteIndex];

				switch (byte) {
				case 'X':
				case 'x':
					goValue |= REG_FLAG_X;
					break;

				case 'Y':
				case 'y':
					goValue |= REG_FLAG_Y;
					break;

				case 'Z':
				case 'z':
					goValue |= REG_FLAG_Z;
					break;
				}
			}

			if (goValue) {
				print("Moving axes: ");
				printRegisterAxes(goValue);
				print(".");

				// Pulse go flags
				STEPDRIVER_mWriteReg(STEPDRIVER_AXI_BASE, REG_AXES_GO_OFFSET, goValue);
				STEPDRIVER_mWriteReg(STEPDRIVER_AXI_BASE, REG_AXES_GO_OFFSET, 0);
			}
			print("\n");
		}

		// Print busy flag changes
		{
			const uint32_t busyFlagsNew = STEPDRIVER_mReadReg(STEPDRIVER_AXI_BASE, REG_AXES_BUSY_OFFSET);
			if (busyFlags != busyFlagsNew) {
				const uint32_t busyFlagsChanged = busyFlags ^ busyFlagsNew;
				const uint32_t axesReadied = busyFlagsChanged & ~busyFlagsNew;
				const uint32_t axesBusied = busyFlagsChanged & busyFlagsNew;
				busyFlags = busyFlagsNew;

				if (axesReadied) {
					print("Axes Ready: ");
					printRegisterAxes(axesReadied);
					print("\n");
				}

				if (axesBusied) {
					print("Axes Busy: ");
					printRegisterAxes(axesBusied);
					print("\n");
				}
			}
		}
	}

	print("ERROR: Command loop terminated.\n");
	return XST_FAILURE;
}




/** Entry point of firmware.
*/
int main(void)
{
	if (UartPs_CfgInitialize_(&uartPS,
		UART_DEVICE_ID, UART_BAUD_RATE) != XST_SUCCESS)
	{
		return XST_FAILURE;
	}
	print("UART initialized.\n");

	if (CommandMenuLoop_() != XST_SUCCESS) {
		print("ERROR: CommandMenuLoop_ failed.\n");
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}
