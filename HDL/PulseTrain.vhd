-- PulseTrain - DataFlow

library IEEE;
use IEEE.std_logic_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.numeric_std.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;




-- 3-Axis pulse generator.
entity PulseTrain is
	generic (
		constant CLOCK_FREQ : natural);

	port (
		clock_i : in std_ulogic;
		reset_i : in std_ulogic;

		buffer_enable_i : in std_ulogic;
		buffer_enable_o : out std_ulogic;

		axes_enable_all_o : out std_ulogic;
		axes_move_i : in std_ulogic_vector(2 downto 0);
		axes_moving_o : out std_ulogic_vector(2 downto 0);
		axes_dir_o : out std_ulogic_vector(2 downto 0);
		axes_step_o : out std_ulogic_vector(2 downto 0);

		status_color_r_o : out std_ulogic;
		status_color_g_o : out std_ulogic;
		status_color_b_o : out std_ulogic);
end entity PulseTrain;




architecture DataFlow of PulseTrain is
	component TimerCountdown is
		generic (
			constant COUNTDOWN_START : natural);

		port (
			clock_i : in std_ulogic;
			enable_i : in std_ulogic;
			done_o : out std_ulogic);
	end component TimerCountdown;

	component PulseTrainAxis is
		generic (
			constant CLOCK_FREQ : natural;
			constant STEPS_PER_REV : natural);

		port (
			clock_i : in std_ulogic;
			enable_i : in std_ulogic;

			move_i : in std_ulogic;
			moving_o : out std_ulogic;

			direction_o : out std_ulogic;
			step_o : out std_ulogic);
	end component PulseTrainAxis;

	signal buffer_enable_s : std_ulogic;
	signal axes_enable_all_s : std_ulogic;

	signal status_color_s : std_ulogic_vector(0 to 2);  -- "RGB"

	constant STEPS_PER_REV : natural := 200;
	constant MICROSTEPS_PER_STEP : natural := 8;
	constant MICROSTEPS_PER_REV : natural
		:= STEPS_PER_REV * MICROSTEPS_PER_STEP;
begin

	-- 3.3V to 5V buffer chip's Output Enable pins
	with ((reset_i = '0') and (buffer_enable_i = '1')) select
		buffer_enable_s <=
			'1' when true,
			'0' when false;
	buffer_enable_o <= buffer_enable_s;

	-- Enable axes some time after the buffer is enabled
	axes_enable_countdown : TimerCountdown
		generic map (
			COUNTDOWN_START => CLOCK_FREQ)  -- 1 second
		port map (
			clock_i => clock_i,
			enable_i => buffer_enable_s,
			done_o => axes_enable_all_s);
	axes_enable_all_o <= axes_enable_all_s;

	axis_x : PulseTrainAxis
		generic map (
			CLOCK_FREQ => CLOCK_FREQ,
			STEPS_PER_REV => MICROSTEPS_PER_REV)

		port map (
			clock_i => clock_i,
			enable_i => axes_enable_all_s,

			move_i => axes_move_i(0),
			moving_o => axes_moving_o(0),

			direction_o => axes_dir_o(0),
			step_o => axes_step_o(0));

	axis_y : PulseTrainAxis
		generic map (
			CLOCK_FREQ => CLOCK_FREQ,
			STEPS_PER_REV => MICROSTEPS_PER_REV)

		port map (
			clock_i => clock_i,
			enable_i => axes_enable_all_s,

			move_i => axes_move_i(1),
			moving_o => axes_moving_o(1),

			direction_o => axes_dir_o(1),
			step_o => axes_step_o(1));

	axis_z : PulseTrainAxis
		generic map (
			CLOCK_FREQ => CLOCK_FREQ,
			STEPS_PER_REV => MICROSTEPS_PER_REV)

		port map (
			clock_i => clock_i,
			enable_i => axes_enable_all_s,

			move_i => axes_move_i(2),
			moving_o => axes_moving_o(2),

			direction_o => axes_dir_o(2),
			step_o => axes_step_o(2));


		status_color_s(0 to 2) <=  -- "RGB"
			"100" when (buffer_enable_s = '0') else  -- Red
			"001" when (axes_enable_all_s = '0') else  -- Blue
			"010" when (axes_enable_all_s = '1') else  -- Green
			"000";  -- Off
		status_color_r_o <= status_color_s(0);
		status_color_g_o <= status_color_s(1);
		status_color_b_o <= status_color_s(2);

end architecture DataFlow;
