library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity StepDriver is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S_AXI
		C_S_AXI_DATA_WIDTH : integer := 32;
		C_S_AXI_ADDR_WIDTH : integer := 4
	);
	port (
		-- Users to add ports here

		-- User ports ends
		clock_i : in std_ulogic;
		
		switches_i : in std_ulogic_vector(3 downto 0);
		buttons_i : in std_ulogic_vector(3 downto 0);
		
		leds_o : out std_ulogic_vector(3 downto 0);
		color_r_o : out std_ulogic;
		color_g_o : out std_ulogic;
		color_b_o : out std_ulogic;
		
		pmod_o : out std_ulogic_vector(7 downto 0);
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S_AXI
		s_axi_aclk : in std_logic;
		s_axi_aresetn : in std_logic;
		s_axi_awaddr : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		s_axi_awprot : in std_logic_vector(2 downto 0);
		s_axi_awvalid : in std_logic;
		s_axi_awready : out std_logic;
		s_axi_wdata : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		s_axi_wstrb : in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		s_axi_wvalid : in std_logic;
		s_axi_wready : out std_logic;
		s_axi_bresp : out std_logic_vector(1 downto 0);
		s_axi_bvalid : out std_logic;
		s_axi_bready : in std_logic;
		s_axi_araddr : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		s_axi_arprot : in std_logic_vector(2 downto 0);
		s_axi_arvalid : in std_logic;
		s_axi_arready : out std_logic;
		s_axi_rdata : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		s_axi_rresp : out std_logic_vector(1 downto 0);
		s_axi_rvalid : out std_logic;
		s_axi_rready : in std_logic
	);
end entity StepDriver;

architecture arch_imp of StepDriver is

	-- component declaration
	component StepDriverAXI is
		generic (
			C_S_AXI_DATA_WIDTH : integer := 32;
			C_S_AXI_ADDR_WIDTH : integer := 4
		);
		port (
			axes_go_o : out std_ulogic_vector(2 downto 0);
			axes_busy_i : in std_ulogic_vector(2 downto 0);

			S_AXI_ACLK : in std_logic;
			S_AXI_ARESETN : in std_logic;
			S_AXI_AWADDR : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
			S_AXI_AWPROT : in std_logic_vector(2 downto 0);
			S_AXI_AWVALID : in std_logic;
			S_AXI_AWREADY : out std_logic;
			S_AXI_WDATA : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
			S_AXI_WSTRB : in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
			S_AXI_WVALID : in std_logic;
			S_AXI_WREADY : out std_logic;
			S_AXI_BRESP : out std_logic_vector(1 downto 0);
			S_AXI_BVALID : out std_logic;
			S_AXI_BREADY : in std_logic;
			S_AXI_ARADDR : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
			S_AXI_ARPROT : in std_logic_vector(2 downto 0);
			S_AXI_ARVALID : in std_logic;
			S_AXI_ARREADY : out std_logic;
			S_AXI_RDATA : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
			S_AXI_RRESP : out std_logic_vector(1 downto 0);
			S_AXI_RVALID : out std_logic;
			S_AXI_RREADY : in std_logic
		);
	end component StepDriverAXI;
	
	
	component PulseTrain is
		generic (
			constant CLOCK_FREQ : natural);

		port (
			clock_i : in std_ulogic;
			reset_i : in std_ulogic;

			buffer_enable_i : in std_ulogic;
			buffer_enable_o : out std_ulogic;

			axes_enable_all_o : out std_ulogic;
			axes_move_i : in std_ulogic_vector(2 downto 0);
			axes_moving_o : out std_ulogic_vector(2 downto 0);
			axes_dir_o : out std_ulogic_vector(2 downto 0);
			axes_step_o : out std_ulogic_vector(2 downto 0);

			status_color_r_o : out std_ulogic;
			status_color_g_o : out std_ulogic;
			status_color_b_o : out std_ulogic);
	end component PulseTrain;


	signal axes_go_s : std_ulogic_vector(2 downto 0);
	signal axes_busy_s : std_ulogic_vector(2 downto 0);

	signal buffer_enable_s : std_ulogic;

	signal axes_enable_all_s : std_ulogic;
	signal axes_dir_s : std_ulogic_vector(2 downto 0);
	signal axes_step_s : std_ulogic_vector(2 downto 0);

begin

	-- Instantiation of Axi Bus Interface S_AXI
	StepDriverAXI_inst : StepDriverAXI
		generic map (
			C_S_AXI_DATA_WIDTH => C_S_AXI_DATA_WIDTH,
			C_S_AXI_ADDR_WIDTH => C_S_AXI_ADDR_WIDTH
		)
		port map (
			axes_go_o(2 downto 0) => axes_go_s(2 downto 0),
			axes_busy_i(2 downto 0) => axes_busy_s(2 downto 0),

			S_AXI_ACLK => s_axi_aclk,
			S_AXI_ARESETN => s_axi_aresetn,
			S_AXI_AWADDR => s_axi_awaddr,
			S_AXI_AWPROT => s_axi_awprot,
			S_AXI_AWVALID => s_axi_awvalid,
			S_AXI_AWREADY => s_axi_awready,
			S_AXI_WDATA => s_axi_wdata,
			S_AXI_WSTRB => s_axi_wstrb,
			S_AXI_WVALID => s_axi_wvalid,
			S_AXI_WREADY => s_axi_wready,
			S_AXI_BRESP => s_axi_bresp,
			S_AXI_BVALID => s_axi_bvalid,
			S_AXI_BREADY => s_axi_bready,
			S_AXI_ARADDR => s_axi_araddr,
			S_AXI_ARPROT => s_axi_arprot,
			S_AXI_ARVALID => s_axi_arvalid,
			S_AXI_ARREADY => s_axi_arready,
			S_AXI_RDATA => s_axi_rdata,
			S_AXI_RRESP => s_axi_rresp,
			S_AXI_RVALID => s_axi_rvalid,
			S_AXI_RREADY => s_axi_rready
		);


	-- Add user logic here
	
	-- Invert Active-Low Logic
	-- 3.3V to 5V buffer chip's Output Enable pins
	pmod_o(0) <= not buffer_enable_s;
	-- Allegro A3977's enable, direction, and step inputs
	pmod_o(1) <= not axes_enable_all_s;
	
	-- X-Axis
	pmod_o(2) <= not axes_dir_s(0);
	pmod_o(3) <= not axes_step_s(0);
	-- Y-Axis
	pmod_o(4) <= not axes_dir_s(1);
	pmod_o(5) <= not axes_step_s(1);
	-- Z-Axis
	pmod_o(6) <= not axes_dir_s(2);
	pmod_o(7) <= not axes_step_s(2);


	pulse_train: PulseTrain
		generic map (
			CLOCK_FREQ => 125000000)
		port map (
			clock_i => clock_i,

			-- Buttons
			reset_i => buttons_i(3),

			-- Switches
			buffer_enable_i => switches_i(3),

			-- LEDs
			axes_moving_o(2 downto 0) => axes_busy_s(2 downto 0),
			status_color_r_o => color_r_o,
			status_color_g_o => color_g_o,
			status_color_b_o => color_b_o,

			-- Pmod Port
			buffer_enable_o => buffer_enable_s,
			axes_enable_all_o => axes_enable_all_s,
			axes_dir_o(2 downto 0) => axes_dir_s(2 downto 0),
			axes_step_o(2 downto 0) => axes_step_s(2 downto 0),

			axes_move_i(2 downto 0) => axes_go_s(2 downto 0));

	leds_o(2 downto 0) <= axes_busy_s(2 downto 0);

	-- User logic ends

end architecture arch_imp;
