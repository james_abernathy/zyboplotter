-- PulseTrainAxis - Behavioral

library IEEE;
use IEEE.std_logic_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;




entity PulseTrainAxis is
	generic (
		constant CLOCK_FREQ : natural;
		constant STEPS_PER_REV : natural);

	port (
		clock_i : in std_ulogic;
		enable_i : in std_ulogic;

		move_i : in std_ulogic;
		moving_o : out std_ulogic;

		direction_o : out std_ulogic;
		step_o : out std_ulogic);
end entity PulseTrainAxis;




architecture Behavioral of PulseTrainAxis is
	signal edges_remaining : natural := 0;
	signal moving_s : boolean;
	signal step_s : std_ulogic;
begin
	moving_s <= (edges_remaining /= 0);
	with (moving_s) select
		moving_o <=
			'1' when true,
			'0' when false;

	direction_o <= '0';
	step_o <= step_s;

	pulser : process(clock_i, enable_i)
		constant STEP_CLOCK_INTERVAL : natural := CLOCK_FREQ / STEPS_PER_REV;
		constant STEP_EDGE_INTERVAL : natural := STEP_CLOCK_INTERVAL / 2;
		variable clocks_until_edge : natural := 0;
	begin
		if (enable_i /= '1') then
			-- Reset
			step_s <= '0';
			edges_remaining <= 0;

		elsif (rising_edge(clock_i)) then
			if ((not moving_s) and (move_i = '1')) then
				-- Begin a full rotation
				clocks_until_edge := 0;
				edges_remaining <= 2 * STEPS_PER_REV;
			end if;

			if (moving_s) then
				-- Step periodically
				if (clocks_until_edge = 0) then
					-- Step edge transition
					clocks_until_edge := STEP_EDGE_INTERVAL - 1;
					edges_remaining <= edges_remaining - 1;

					step_s <= not step_s;

				else
					-- Wait for next step edge
					clocks_until_edge := clocks_until_edge - 1;
				end if;
			end if;
		end if;
	end process pulser;

end architecture Behavioral;
