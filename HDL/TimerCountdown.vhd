-- TimerCountdown - Behavioral

library IEEE;
use IEEE.std_logic_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;




-- Timer to wait for a time interval and then assert done_o.
entity TimerCountdown is
	generic (
		constant COUNTDOWN_START : natural);
			
	port (
		clock_i : in std_ulogic;
		enable_i : in std_ulogic;
		done_o : out std_ulogic);
end entity TimerCountdown;




architecture Behavioral of TimerCountdown is
	signal count_s : natural := COUNTDOWN_START;
	signal done_s : boolean;
begin
	-- Signal done when countdown reaches zero.
	done_s <= (count_s = 0);
	with (done_s) select
		done_o <=
			'1' when true,
			'0' when false;

	-- Counts down while enabled and not done_s.
	Countdown : process(clock_i, enable_i)
	begin
		if (enable_i /= '1') then
			count_s <= COUNTDOWN_START;
				
		elsif (rising_edge(clock_i)) then
			if (not done_s) then
				-- Tick down
				count_s <= count_s - 1;
			end if;
		end if;
	end process Countdown;

end architecture Behavioral;
